/************************************************************************
> File Name: lisklist.c 
> Author:    Joe 
> Mail:      1119111180@qq.com 
> Time:      19/01/24 
> Desc:      
************************************************************************/ 


struct single_list list {
    single_list *next;
    int val;
}

// 反转
void reverse(struct single_list *head) {
    struct single_list *next, *prev = NULL;

    while (head) {
        next = head->next;
        head->next = prev;
        prev = head;
        head= next;
    }
}

// 中间节点
void middle(struct single_list *head) {
    struct single_list *fast = head = NULL;
    struct single_list *slow = head = NULL;

    while (fast != NULL && fast->next != NULL) {
        slow = slow->next;
        fast = fast->next->next;
    }
    return slow;
}

// 删除倒数第n个节点

struct single_list *delete(struct single_list *head, int n) {
    struct single_list *fast = head;
    struct single_list *prev = NULL;
    struct single_list *next = head;

    while ((n > 1) && fast != NULL) {
        fast = fast->next;
        n--;
    }

    // 说明链表数目不足n个
    if (fast == NULL) {
        return head;
    }

    while (fast->next != NULL) {
        fast = fast->next;
        prev = next;
        next = next->next;
    }

    if (prev == NULL) 
        head = head->next;
    else 
        prev->next = prev->next->next;
    
    return head;
}


// 合并有序链表

struct single_list *merge(struct single_list *l1, struct single_list *l2) {
    struct single_list head = {0};
    struct single_list *phead = &head;

    while (1) {
        if (l1 == NULL)
            phead->next = l2;
        
        if (l2 == NULL)
            phead->next = l1;

        if (l1->val < l2->val) {
            phead->next = l1;
            l1 = l1->next;
        } else {
            phead->next = l2;
            l2 = l2->next;
        }
        phead = phead->next;
    }
    return head;
} 